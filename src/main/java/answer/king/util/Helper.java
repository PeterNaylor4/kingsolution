/**
 * 
 */
package answer.king.util;

import java.math.BigDecimal;
import java.util.List;

import answer.king.model.Item;
import answer.king.model.LineItem;
import answer.king.model.Order;

/**
 * @author Pete Simple helper class for the king app
 */
public class Helper {
    private final static String EMPTY_STRING = "";

    public static boolean isBlankOrNull(Item item) {
        if (item == null || Helper.isBlankOrNull(item.getName()) || Helper.isBlankOrNull(item.getPrice())) {
            return (true);
        }

        return (false);
    }

    public static boolean isBlankOrNull(Order order) {
        if (order == null) {
            return (true);
        }

        return (false);
    }

    public static boolean isBlankOrNull(String aString) {
        if (aString == null || aString.equalsIgnoreCase(Helper.EMPTY_STRING)) {
            return (true);
        }

        return (false);
    }

    public static boolean isBlankOrNull(Long aLong) {
        if (aLong == null || aLong <= 0) {
            return (true);
        }

        return (false);
    }

    public static boolean isBlankOrNull(BigDecimal aBigDecimal) {
        if (aBigDecimal == null || aBigDecimal.doubleValue() <= 0) {
            return (true);
        }

        return (false);
    }

    public static boolean isBlankOrNull(int anInt) {
        return (false);
    }

    public static boolean isBlankOrNull(List<LineItem> lineItems) {
        if (lineItems == null || lineItems.size() <= 0) {
            return (true);
        }

        return (false);
    }
}
