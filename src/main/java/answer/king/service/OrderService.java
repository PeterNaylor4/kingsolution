package answer.king.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import answer.king.model.Item;
import answer.king.model.LineItem;
import answer.king.model.Order;
import answer.king.model.Receipt;
import answer.king.repo.ItemRepository;
import answer.king.repo.LineItemRepository;
import answer.king.repo.OrderRepository;
import answer.king.util.Helper;

@Service
@Transactional
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private LineItemRepository lineItemRepository;

    @Autowired
    ReceiptService receiptService;

    public List<Order> getAll() {
        return orderRepository.findAll();
    }

    public Order save(Order order) {
        return orderRepository.save(order);
    }

    /**
     * 
     * @param id Long the order id
     * @param itemId Long the item id
     * @param qty int the number to order
     * @return order ORder, the actual order
     */
    public Order addItem(Long id, Long itemId, int qty) {
        Order order = orderRepository.findOne(id);
        boolean found = false;
        List<LineItem> existingLineItems = order.getLineItems();

        // OK, check for existing line items; if existing line items then do an addition
        if (!Helper.isBlankOrNull(existingLineItems)) {
            for (int looper = 0; looper < existingLineItems.size(); looper++) {
                LineItem existingLineItem = existingLineItems.get(0);

                // check if its the same one coming in
                if (existingLineItem.getItemId().compareTo(itemId) == 0) {
                    // then just up the quantity on this item
                    existingLineItem.setQuantity(existingLineItem.getQuantity() + qty);
                    lineItemRepository.save(existingLineItem);
                    found = true;

                    break;
                }
            }
        }

        // not found, create a new one
        if (!found) {
            createNewLineItemOrder(order, itemId, qty);
        }

        // save the order
        orderRepository.save(order);

        return (order);
    }

    private void createNewLineItemOrder(Order order, Long itemId, int qty) {
        Item item = itemRepository.findOne(itemId);
        LineItem lineItem = createLineItem(item, qty);
        lineItem.setOrder(order);

        order.getLineItems().add(lineItem);
    }

    private LineItem createLineItem(Item item, int qty) {
        LineItem lineItem = new LineItem();
        lineItem.setItemId(item.getId());
        lineItem.setQuantity(qty);
        lineItem.setOriginalPrice(item.getPrice());
        lineItemRepository.save(lineItem);

        return (lineItem);
    }

    /**
     * pay for an order, returns null if the payment was incorrect
     * @param id
     * @param payment
     * @return null or a Receipt object
     **/
    public BigDecimal pay(Long id, BigDecimal payment) {
        Order order = orderRepository.findOne(id);
        BigDecimal totalOrderValue = new BigDecimal(0);
        BigDecimal change = new BigDecimal(0);
        Receipt receipt = receiptService.save(new Receipt());

        // total up the order for payment checking, we could use order.getItems().stream().map(Item::getPrice).red....
        // but lets be clearer
        for (int looper = 0; looper < order.getLineItems().size(); looper++) {
            // get the total line item price first, get quantity first NOTE: cold make this a BigDecimal in the system
            BigDecimal qty = new BigDecimal(order.getLineItems().get(looper).getQuantity());
            BigDecimal totalLineItemValue = order.getLineItems().get(looper).getOriginalPrice().multiply(qty);
            totalOrderValue = totalOrderValue.add(totalLineItemValue);
        }

        if (payment.compareTo(totalOrderValue) == -1) { // less than total payment, return null as error indicator
            change = null;
        } else {
            // set the payment and the order
            receipt.setPayment(payment);
            // set the paid flag
            order.setPaid(true);
            receipt.setOrder(order);

            // get change if any due
            change = receipt.getChangeFromSubmittedPayment();

            // add the receipt to the order
            if (order.getReceipts() == null) {
                // then create receipts array
                List<Receipt> receipts = new ArrayList<Receipt>();
                receipts.add(receipt);
                order.setReceipts(receipts);
            } else {
                order.getReceipts().add(receipt);
            }

            // connect to the order
            receipt.setOrder(order);

            // save everything
            receiptService.save(receipt);
            orderRepository.save(order);
        }

        return (change);
    }
}
