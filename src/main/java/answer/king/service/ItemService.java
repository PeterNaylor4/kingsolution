package answer.king.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import answer.king.model.Item;
import answer.king.repo.ItemRepository;
import answer.king.util.Helper;

@Service
@Transactional
public class ItemService {

    @Autowired
    private ItemRepository itemRepository;

    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    public Item save(Item item) {
        Item returnItem = null;

        // make sure the item is valid
        if (isValid(item) == true) {
            returnItem = itemRepository.save(item);
        }

        return (returnItem);
    }

    /**
     * change the price of an existing item
     * @param itemId Long
     * @param newPrice BigDecimal
     */
    public Item changeItemPrice(Long itemId, BigDecimal newPrice) {
        // OK, validation, let's assume (without and guidance as of yet) that we can't have null or 0 price tags
        if (Helper.isBlankOrNull(itemId) || Helper.isBlankOrNull(newPrice)) {
            return (null);
        }

        Item item = itemRepository.findOne(itemId);
        item.setPrice(newPrice);

        // now save the item
        itemRepository.save(item);

        // might look unnecessary but better to check it's in the in-mem DB!
        return (itemRepository.findOne(itemId));
    }

    /**
     * Check if an item is valid. Assume as of now that validity means the item mandates a name, price and id; Further
     * validation would be to check if the item already exists in the DB but this is currently controlled by hibernate
     * 
     * @param item an Item object
     * @return isValid Boolean true or false
     **/
    private boolean isValid(Item item) {
        // check if null or empty etc
        if (Helper.isBlankOrNull(item)) {
            return (false);
        }

        return (true);
    }

    /* private boolean existsAlready(Item item) { List<Item> currentItemsList = this.getAll(); for (int looper = 0;
     * looper < currentItemsList.size(); looper++) { Item currentItem = currentItemsList.get(looper); // assume we can't
     * have null or empty items here for now if (item.getId().longValue() == currentItem.getId().longValue()) { // item
     * exists already return (true); } }
     * 
     * return (false); } */

}
