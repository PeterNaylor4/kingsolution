package answer.king.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "T_RECEIPT")
public class Receipt {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private BigDecimal payment;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY) // expresses many receipts to 1 order
    @JoinColumn(name = "ORDER_ID")
    private Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getChange() {
        BigDecimal totalOrderPrice = order.getLineItems().stream().map(LineItem::getOriginalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return payment.subtract(totalOrderPrice);
    }

    public BigDecimal getChangeFromSubmittedPayment() {
        BigDecimal totalOrderValue = new BigDecimal(0);

        for (int looper = 0; looper < order.getLineItems().size(); looper++) {
            // get the total line item price first, get quantity first NOTE: cold make this a BigDecimal in the system
            BigDecimal qty = new BigDecimal(order.getLineItems().get(looper).getQuantity());
            BigDecimal totalLineItemValue = order.getLineItems().get(looper).getOriginalPrice().multiply(qty);
            totalOrderValue = totalOrderValue.add(totalLineItemValue);
        }

        return (payment.subtract(totalOrderValue));
    }
}
