package answer.king.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import answer.king.model.Receipt;
import answer.king.service.ReceiptService;

@RestController
@RequestMapping("/receipt")
public class ReceiptController {

    @Autowired
    private ReceiptService receiptService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Receipt> getAll() {
        return receiptService.getAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Receipt create() {
        return receiptService.save(new Receipt());
    }
}
