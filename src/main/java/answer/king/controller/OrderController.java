package answer.king.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import answer.king.model.Order;
import answer.king.service.OrderService;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Order> getAll() {
        return orderService.getAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Order create() {
        return orderService.save(new Order());
    }

    @RequestMapping(value = "/{id}/addItem/{itemId}/{qty}", method = RequestMethod.PUT)
    public Order addItem(@PathVariable("id") Long id, @PathVariable("itemId") Long itemId,
            @PathVariable("qty") int qty) {
        Order newORderDetails = orderService.addItem(id, itemId, qty);

        return (newORderDetails);
    }

    @RequestMapping(value = "/pay/{id}/{payment}", method = RequestMethod.PUT)
    public BigDecimal pay(@PathVariable("id") Long id, @PathVariable BigDecimal payment) {
        return orderService.pay(id, payment);
    }
}
