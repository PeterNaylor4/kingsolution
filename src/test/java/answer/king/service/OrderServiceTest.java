package answer.king.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import answer.king.fixtures.OrderFixture;
import answer.king.model.Order;
import answer.king.model.Receipt;
import answer.king.repo.OrderRepository;
import answer.king.repo.ReceiptRepository;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ReceiptService receiptService;

    @Mock
    private ReceiptRepository receiptRepository;

    /** service implementation to test. */
    @InjectMocks
    private OrderService orderService = new OrderService();

    @Before
    public final void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public final void testPayOrderSuccess() throws Exception {
        Order order = OrderFixture.getPaidOrder();
        BigDecimal payment = new BigDecimal(66); // this is the 21 + 22 + 23 values of the 3 items
        Receipt receipt = new Receipt();

        // mocks
        when(orderRepository.findOne(order.getId())).thenReturn(order);
        when(receiptService.save(any())).thenReturn(receipt);

        // call the service
        BigDecimal change = orderService.pay(order.getId(), payment);
        // verify mock invocations
        verify(orderRepository, times(1)).findOne(order.getId());

        // asserts
        assertNotNull("Should not be null", change);
        assertEquals("Wrong value", change.compareTo(new BigDecimal(0)), 0);
    }

    /**
     * if payment was not made due to insufficient funds, null change is returned, otherwise 0 or actual change
     * @throws Exception
     */
    @Test
    public final void testPayOrderFail() throws Exception {
        Order order = OrderFixture.getPaidOrder();
        BigDecimal payment = new BigDecimal(0.88);

        // mocks
        when(orderRepository.findOne(order.getId())).thenReturn(order);
        when(receiptService.save(any())).thenReturn(new Receipt());

        // call the service
        BigDecimal change = orderService.pay(order.getId(), payment);
        // verify mock invocations
        verify(orderRepository, times(1)).findOne(order.getId());

        // asserts
        assertNull("Should be null", change);
    }
}