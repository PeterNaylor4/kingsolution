package answer.king.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import answer.king.fixtures.ItemFixture;
import answer.king.model.Item;
import answer.king.repo.ItemRepository;
import answer.king.util.Helper;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {

    @Mock
    private ItemRepository itemRepository;

    /** service implementation to test. */
    @InjectMocks
    private ItemService itemService = new ItemService();

    @Before
    public final void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Testing get all items
     * @throws Exception
     **/
    @Test
    public final void testGetAllSuccess() throws Exception {
        List<Item> itemsList = new ArrayList<Item>();
        itemsList = ItemFixture.getItemsList(10);
        // mocks
        when(itemRepository.findAll()).thenReturn(itemsList);

        // call the service
        List<Item> returnItemsList = itemService.getAll();
        // verify mock invocations
        verify(itemRepository).findAll();

        // asserts
        assertNotNull("Should not be null", returnItemsList);
        assertTrue("List wrong size", returnItemsList.size() == itemsList.size());
    }

    /**
     * Testing save item
     * @throws Exception
     **/
    @Test
    public final void testCreateSaveItem() throws Exception {
        Item item = ItemFixture.getItemsList(1).get(0);
        Item returnItem = ItemFixture.getItemsList(1).get(0);
        Long firstRecord = new Long(1);
        Long secondRecord = new Long(2);

        // mocks
        when(itemRepository.save(item)).thenReturn(returnItem);
        // call the service
        Item returnedItem = itemService.save(item);
        // verify mock invocations
        verify(itemRepository, times(1)).save(item);

        // asserts
        assertNotNull("Should not be null", returnedItem);
        assertEquals("Incorect data", returnedItem.getId(), firstRecord);

        // lets do it again and check for id
        returnItem.setId(secondRecord);
        when(itemRepository.save(item)).thenReturn(returnItem);
        returnedItem = itemService.save(item);
        assertEquals("Incorect data", returnedItem.getId(), secondRecord);
    }

    /**
     * Testing dave item of invalid items
     * @throws Exception
     **/
    @Test
    public final void testCreateSaveInvalidItem() throws Exception {
        Item item = ItemFixture.getItemsList(1).get(0);
        // invalidate this item by setting one of it's elements to null etc.
        item.setName(null);

        // call the service
        Item returnedItem = itemService.save(item);
        // verify mock invocations
        verify(itemRepository, times(0)).save(item);

        // asserts
        assertNull("Should be null", returnedItem);
        assertEquals("Should be empty", Helper.isBlankOrNull(returnedItem), true);

        // check with no price
        item.setPrice(null);
        item.setName("Testing");
        returnedItem = itemService.save(item);
        // verify mock invocations
        verify(itemRepository, times(0)).save(item);

        // asserts
        assertNull("Should be null", returnedItem);
        assertEquals("Should be empty", Helper.isBlankOrNull(returnedItem), true);

        // check for an empty name
        item.setPrice(new BigDecimal(66.33));
        item.setName("");
        returnedItem = itemService.save(item);
        // verify mock invocations
        verify(itemRepository, times(0)).save(item);

        // asserts
        assertNull("Should be null", returnedItem);
        assertEquals("Should be empty", Helper.isBlankOrNull(returnedItem), true);
    }

    /**
     * Testing item price changes
     * @throws Exception
     */
    @Test
    public final void testChangeItemPriceSuccess() throws Exception {
        Item item = ItemFixture.getItemsList(1).get(0);
        Item returnItem = ItemFixture.getItemsList(1).get(0);
        BigDecimal newPrice = new BigDecimal(22.22);

        // mocks
        when(itemRepository.findOne(item.getId())).thenReturn(returnItem);
        // call the service
        Item returnedItem = itemService.changeItemPrice(item.getId(), newPrice);
        // verify mock invocations
        verify(itemRepository, times(2)).findOne(item.getId());

        // asserts
        assertNotNull("Should not be null", returnedItem);
        assertEquals("Incorect data", returnedItem.getPrice(), newPrice);
    }

    @Test
    public final void testChangeItemPriceFailures() throws Exception {
        Item item = ItemFixture.getItemsList(1).get(0);
        BigDecimal newPrice = new BigDecimal(0.00);

        // call the service checking for 0 value being passed for price
        Item returnedItem = itemService.changeItemPrice(item.getId(), newPrice);
        // verify mock invocations, should never be called
        verify(itemRepository, times(0)).findOne(item.getId());

        // asserts
        assertNull("Should be null", returnedItem);

        // call the service checking for null value being passed for price
        newPrice = null;
        returnedItem = itemService.changeItemPrice(item.getId(), newPrice);
        // verify mock invocations, should never be called
        verify(itemRepository, times(0)).findOne(item.getId());

        // asserts
        assertNull("Should be null", returnedItem);
    }
}