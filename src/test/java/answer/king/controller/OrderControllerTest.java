package answer.king.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import answer.king.fixtures.OrderFixture;
import answer.king.model.Order;
import answer.king.service.OrderService;

public class OrderControllerTest {
    @Mock
    private OrderService orderService;

    /** Controller under test. */
    @InjectMocks
    private OrderController controller = new OrderController();

    @Before
    public final void setup() {
        // initialise mocks
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test successful payment of an order
     **/
    @Test
    public final void testPayOrderSuccess() throws Exception {
        Order order = OrderFixture.getPaidOrder();
        BigDecimal payment = new BigDecimal(88.88);
        BigDecimal change = new BigDecimal(0);

        // mocks
        when(orderService.pay(order.getId(), payment)).thenReturn(change);
        // call the service
        BigDecimal returnChange = controller.pay(order.getId(), payment);

        // verify mock invocation numbers
        verify(orderService, times(1)).pay(order.getId(), payment);
        assertEquals("Incorrect return value", returnChange.compareTo(change) > 0, Boolean.FALSE);
    }

    /**
     * Test unsuccessful payment of an order
     **/
    @Test
    public final void testPayOrderFail() throws Exception {
        Order order = OrderFixture.getPaidOrder();
        BigDecimal payment = new BigDecimal(88.88);
        BigDecimal change = new BigDecimal(0);

        // mocks
        when(orderService.pay(order.getId(), payment)).thenReturn(change);
        // call the service
        BigDecimal returnChange = controller.pay(order.getId(), payment);

        // verify mock invocation numbers
        verify(orderService, times(1)).pay(order.getId(), payment);
        assertEquals("Incorrect return value", returnChange.compareTo(change) > 0, Boolean.FALSE);
    }

    /**
     * Successful call of multiple orders
     * @throws Exception
     **/
    @Test
    public final void testORdersSuccess() throws Exception {
        List<Order> ordersList = OrderFixture.getOrdersList(10);
        List<Order> responseOrdersList = null;
        // mocks
        when(orderService.getAll()).thenReturn(ordersList);
        // call the service
        responseOrdersList = controller.getAll();

        // verify number of invocations
        verify(orderService, times(1)).getAll();

        // check results
        assertEquals("Incorrect response", 10, responseOrdersList.size());
        assertEquals("Incorrect values", ordersList.get(5).getId(), responseOrdersList.get(5).getId());
    }

    /**
     * Test single order
     **/
    @Test
    public final void testGetOrdersingleItemReturned() throws Exception {
        List<Order> ordersList = OrderFixture.getOrdersList(1);

        when(orderService.getAll()).thenReturn(ordersList);

        // call the service
        List<Order> responseOrdersList = controller.getAll();

        // verify mock invocation numbers
        verify(orderService, times(1)).getAll();

        assertEquals("Incorrect response", ordersList.size(), responseOrdersList.size());
        assertEquals("Incorrect id", ordersList.get(0).getId(), responseOrdersList.get(0).getId());
        assertEquals("Incorrect paid value", ordersList.get(0).getPaid(), responseOrdersList.get(0).getPaid());
    }

    /**
     * Test null return of orders
     **/
    @Test
    public final void testGetOrdersNUll() throws Exception {
        List<Order> ordersList = null;
        when(orderService.getAll()).thenReturn(ordersList);

        // call the service
        List<Order> responseOrdersList = controller.getAll();

        // verify mock invocation numbers
        verify(orderService, times(1)).getAll();

        assertNull(responseOrdersList);
    }
}
