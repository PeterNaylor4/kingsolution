package answer.king.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import answer.king.fixtures.ItemFixture;
import answer.king.model.Item;
import answer.king.service.ItemService;

public class ItemControllerTest {
    @Mock
    private ItemService itemService;

    /** Controller under test. */
    @InjectMocks
    private ItemController controller = new ItemController();

    @Before
    public final void setup() {
        // initialise mocks
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Successful call of multiple items
     * @throws Exception
     **/
    @Test
    public final void testGetPartsSuccess() throws Exception {
        List<Item> itemsList = ItemFixture.getItemsList(10);
        List<Item> responseItemsList = null;
        // mocks
        when(itemService.getAll()).thenReturn(itemsList);
        // call the service
        responseItemsList = controller.getAll();

        // verify number of invocations
        verify(itemService, times(1)).getAll();

        // check results
        assertEquals("Incorrect response", itemsList.size(), responseItemsList.size());
        assertEquals("Incorrect values", itemsList.get(5).getId(), responseItemsList.get(5).getId());
    }

    /**
     * Test single item
     **/
    @Test
    public final void testGetItemsSingleItemReturned() throws Exception {
        List<Item> itemsList = ItemFixture.getItemsList(1);

        when(itemService.getAll()).thenReturn(itemsList);

        // call the service
        List<Item> responseItemsList = controller.getAll();

        // verify mock invocation numbers
        verify(itemService, times(1)).getAll();

        assertEquals("Incorrect response", itemsList.size(), responseItemsList.size());
        assertEquals("Incorrect values", itemsList.get(0).getName(), responseItemsList.get(0).getName());
    }

    /**
     * This tests invalid data sent to save item - Tests null return of items (null indicates error as of now)
     **/
    @Test
    public final void testGetItemsNUll() throws Exception {
        List<Item> items = null;
        when(itemService.getAll()).thenReturn(items);

        // call the service
        List<Item> responseItemsList = controller.getAll();

        // verify mock invocation numbers
        verify(itemService, times(1)).getAll();

        assertNull(responseItemsList);
    }

    /**
     * Test save of items
     **/
    @Test
    public final void testSaveItems() throws Exception {
        Item postItem = ItemFixture.getItemsList(1).get(0);
        when(itemService.save(postItem)).thenReturn(postItem);

        // call the service
        Item responseItem = controller.create(postItem);

        // verify mock invocation numbers
        verify(itemService, times(1)).save(postItem);

        assertEquals("Incorrect size", postItem.getId(), responseItem.getId());
    }

    /**
     * Test changing of an items price
     **/
    @Test
    public final void testItemPrice() throws Exception {
        Item postItem = ItemFixture.getItemsList(1).get(0);
        BigDecimal newPrice = new BigDecimal(22.22);
        Long itemId = Long.getLong("1");
        when(itemService.changeItemPrice(itemId, newPrice)).thenReturn(postItem);

        // call the service
        Item responseItem = controller.changeItemPrice(itemId, newPrice);

        // verify mock invocation numbers
        verify(itemService, times(1)).changeItemPrice(itemId, newPrice);

        assertEquals("Incorrect data", postItem.getPrice(), responseItem.getPrice());
    }
}
