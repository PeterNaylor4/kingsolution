package answer.king.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import answer.king.fixtures.ItemFixture;
import answer.king.model.Item;
import answer.king.model.LineItem;
import answer.king.model.Order;
import answer.king.model.Receipt;
import answer.king.repo.LineItemRepository;
import answer.king.repo.OrderRepository;
import answer.king.repo.ReceiptRepository;
import answer.king.service.ItemService;
import answer.king.service.OrderService;

/**
 * @author Pete
 * 
 *         This class tests end to end integration, data persistence and payments and receipts
 *
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class OrderControllerIntTest {
    @Autowired
    ItemService itemService;

    @Autowired
    OrderService orderService;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ReceiptRepository receiptRepository;

    @Autowired
    LineItemRepository lineItemRepository;

    @Test
    public void testPay() {
        // create and save order
        Order order = new Order();
        orderService.save(order);
        BigDecimal payment = new BigDecimal(100);

        // ensure all is well
        assertNotNull(order.getId());
        assertTrue(order.getId() > 0);
        assertEquals(orderRepository.findAll().size(), 1);

        // create and save receipt
        BigDecimal change = orderService.pay(order.getId(), payment);
        assertTrue(change.compareTo(new BigDecimal(0)) > 0);

        // check is in repository not just returned from service; only 1 so get index 0
        assertEquals(receiptRepository.findAll().size(), 1);
        Receipt saved = receiptRepository.findAll().get(0);
        assertNotNull("No data in DB", saved);

        // check for line items as well, should be 0 as of now
        List<LineItem> lineItems = lineItemRepository.findAll();
        assertEquals(lineItems.size(), 0);

        // check the order is there and values are correct
        Order savedOrder = orderRepository.findOne(order.getId());
        assertTrue("Values not equal", change.compareTo(payment) == 0);
        assertTrue("Not paid when it should be", savedOrder.getPaid() == true);
    }

    // 1. Run through creating 4 items
    // 2. Create a 2nd order (persisted data from first test above)
    // 3. Add 2 items to this order (proving 2nd order exists) 1 and 3 @ qty's of 2 and 4 respectively
    // 4. Total order price will be 134
    // 5. Make a payment of 200 to checking change returned = 66
    // 6. Retrieve the order and check it has been paid proving paid flag set to TRUE and persisted
    @Test
    public void testPayFullFunctionality() {
        BigDecimal payment = new BigDecimal(200);
        BigDecimal expectedChange = new BigDecimal(66.00); // 200 payment minus actual order value of 134

        // create and save items
        List<Item> items = ItemFixture.getItemsList(4);
        for (int itemLooper = 0; itemLooper < items.size(); itemLooper++) {
            itemService.save(items.get(itemLooper));
        }

        // create and save order
        Order order = new Order();
        orderService.save(order);

        // add items to order
        orderService.addItem(new Long(2), new Long(1), 2);
        orderService.addItem(new Long(2), new Long(3), 4);

        // ensure all is well
        assertNotNull(order.getId());
        assertTrue(order.getId() > 0);
        List<Order> orders = orderRepository.findAll();
        assertEquals(orders.size(), 2);

        // create and save receipt
        BigDecimal change = orderService.pay(order.getId(), payment);
        assertTrue(change.compareTo(new BigDecimal(0)) > 0);
        assertTrue("Values not equal", change.compareTo(payment) == -1);
        assertTrue("Incorrect change returned", change.compareTo(expectedChange) == 0);

        // check is in repository not just returned from service
        assertEquals(receiptRepository.findAll().size(), 2);
        Receipt saved = receiptRepository.findAll().get(0);
        assertNotNull("No data in DB", saved);

        // check for line items as well, should be 0 as of now
        List<LineItem> lineItems = lineItemRepository.findAll();
        assertEquals(lineItems.size(), 2);

        // check the order is there and values are correct
        Order savedOrder = orderRepository.findOne(order.getId());
        assertTrue("Not marked as paid when it should be", savedOrder.getPaid() == true);
    }

    /**
     * Test adding an existing item in an order so that the qty is increased and no new line item is added Use order 1
     * which has qty 0 of items so... add 2, then another 2, making item 1's qty 4, then check for 4
     **/
    @Test
    public void testPayOrderAddition() {
        Long orderId = new Long(1);
        Long itemId = new Long(1);
        int expectedQuantity = 4;

        // add 2 to the order
        orderService.addItem(orderId, itemId, 2);
        // now add another 2 to create 4 in the same line item
        Order latestOrder = orderService.addItem(orderId, itemId, 2);

        // OK, all should be saved now, check it all from scratch
        List<LineItem> lineItems = latestOrder.getLineItems();

        for (int looper = 0; looper < lineItems.size(); looper++) {
            LineItem lineItem = lineItems.get(looper);

            if (lineItem.getItemId().compareTo(itemId) == 0) {
                System.out.println(lineItem.getQuantity());
                assertEquals("item not increased as expected", lineItem.getQuantity(), expectedQuantity);
            }
        }

    }

    // 1. OK, we have two orders in here so lets check the receipts exist for them and their respective data is correct
    @Test
    public void testReceipts() {
        BigDecimal firstPayment = new BigDecimal(100);
        BigDecimal secondPayment = new BigDecimal(200);
        List<Receipt> receipts = receiptRepository.findAll();

        System.out.println(receipts);
        assertEquals("Incorrect number of receipts in system", receipts.size(), 2);

        // 100 was paid to order 1, 200 to order 2, check the values but use getters for the orders
        Receipt orderOneReceipt = receiptRepository.findOne((long) 1);
        Receipt orderTwoReceipt = receiptRepository.findOne((long) 2);
        // validate data
        assertTrue("Incorrect payment", orderOneReceipt.getPayment().compareTo(firstPayment) == 0);
        assertTrue("Incorrect payment", orderTwoReceipt.getPayment().compareTo(secondPayment) == 0);

    }

}
