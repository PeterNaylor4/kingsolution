package answer.king.fixtures;

import java.util.ArrayList;
import java.util.List;

import answer.king.model.Item;
import answer.king.model.LineItem;

public class LineItemFixture {

    public static List<LineItem> getLineItemsList(int numberToGet) {
        List<LineItem> lineItemsList = new ArrayList<LineItem>();

        // map results
        for (int count = 0; count < numberToGet; count++) {
            Item item = ItemFixture.getItemsList(count + 1).get(count);

            LineItem lineItem = new LineItem();
            lineItem.setId(new Long(count + 1));
            lineItem.setItemId(item.getId());
            lineItem.setQuantity(1);
            lineItem.setOriginalPrice(item.getPrice());
            // lineItem.setOrder(order);

            lineItemsList.add(lineItem);
        }

        return (lineItemsList);
    }

    public static void main(String args[]) {
        List<LineItem> lineItemsList = LineItemFixture.getLineItemsList(10);
        System.out.println(lineItemsList);
    }
}