package answer.king.fixtures;

import java.math.BigDecimal;

import answer.king.model.Receipt;

// @SuppressWarnings("unchecked")
public class ReceiptFixture {

    public static Receipt getUnpaidReceipt() {
        Receipt receipt = new Receipt();
        receipt.setOrder(OrderFixture.getOrdersList(1).get(0));
        receipt.setPayment(new BigDecimal(88.88));

        return (receipt);
    }

    public static Receipt getPaidReceipt() {
        Receipt receipt = new Receipt();
        receipt.setOrder(OrderFixture.getOrdersList(1).get(0));
        receipt.getOrder().setPaid(true);
        receipt.setPayment(new BigDecimal(88.88));

        return (receipt);
    }

    public static void main(String args[]) {
        Receipt receipt = ReceiptFixture.getUnpaidReceipt();
        System.out.println(receipt);
    }
}