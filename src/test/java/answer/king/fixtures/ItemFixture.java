package answer.king.fixtures;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import answer.king.model.Item;

// @SuppressWarnings("unchecked")
public class ItemFixture {

    public static List<Item> getItemsList(int numberToGet) {
        List<Item> itemsList = new ArrayList<Item>();

        // map results
        for (int count = 0; count < numberToGet; count++) {
            Item item = new Item();
            item.setId(new Long(count + 1));
            item.setName("Name " + count);
            item.setPrice(new BigDecimal(20.00 + (count + 1)));

            itemsList.add(item);
        }

        return (itemsList);
    }

    public static void main(String args[]) {
        List<Item> itemsList = ItemFixture.getItemsList(10);
        System.out.println(itemsList);
    }
}