package answer.king.fixtures;

import java.util.ArrayList;
import java.util.List;

import answer.king.model.Order;
import answer.king.model.Receipt;

public class OrderFixture {

    public static List<Order> getOrdersList(int numberToGet) {
        List<Order> ordersList = new ArrayList<Order>();

        // map results
        for (int count = 0; count < numberToGet; count++) {
            Order order = new Order();
            order.setId(new Long(count));
            order.setLineItems(LineItemFixture.getLineItemsList(3));
            order.setPaid(false);

            ordersList.add(order);
        }

        return (ordersList);
    }

    public static Order getPaidOrder() {
        Order order = new Order();
        order.setId(new Long(55));
        order.setLineItems(LineItemFixture.getLineItemsList(3));
        List<Receipt> receipts = new ArrayList<Receipt>();
        receipts.add(ReceiptFixture.getPaidReceipt());
        order.setReceipts(receipts);
        order.setPaid(true);

        return (order);
    }

    public static void main(String args[]) {
        List<Order> ordersList = OrderFixture.getOrdersList(10);
        System.out.println(ordersList);
    }
}