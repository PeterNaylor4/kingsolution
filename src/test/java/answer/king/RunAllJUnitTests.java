package answer.king;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import answer.king.controller.ItemControllerTest;
import answer.king.controller.OrderControllerTest;
import answer.king.service.ItemServiceTest;
import answer.king.service.OrderServiceTest;;

@RunWith(Suite.class)
@SuiteClasses({ ItemControllerTest.class, OrderControllerTest.class, ItemServiceTest.class, OrderServiceTest.class })
/**
 * @author UKX8804 Will run all JUnit test classes (and all their tests) detailed in the above @SuiteClasses list
 **/
public class RunAllJUnitTests {
}
